﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JourneyToHermes
{
    class Camera
    {
        public Matrix transformationMatrix;
        public Vector2 position; 

        //basic constructor for the camera
        public Camera()
        {
            position = Vector2.Zero;
        }

        //moves the camera by the amount
        public void Move(Vector2 amount)
        {
            position = amount;
        }

        //accessor for the camera position
        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

        //actually moves the camera
        public Matrix GetNewTransform(GraphicsDevice graphicsDevice)
        {
            transformationMatrix = Matrix.CreateTranslation(new Vector3(-position.X, -position.Y, 0)) * Matrix.CreateTranslation(new Vector3(1360 * 0.5f, 0, 0));
            return transformationMatrix;
        }
    }
}
