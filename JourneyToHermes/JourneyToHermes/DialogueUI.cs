﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input; //needed for XNA

namespace JourneyToHermes
{
    class DialogueUI
    {
        //Attributes for dialogue
        Texture2D menu;
        SpriteFont dialogueText;
        KeyboardState kState;
        KeyboardState kStatePrev;
        int dialogueCount = 0;
        public bool IsDialogueDone = false;
        Dictionary<int, string[]> dialogue = new Dictionary<int, string[]>();
        public int dialogueToPlay = 0;
        // game state for later, need to apply to dialogue class
        enum GameState { DialogueTutorial, Playing, Dialogue2, Dialogue3 }
        GameState gState;

        //array and attributes for tutorial dialogue
        string[] d1 = new string[9] { "Press enter to continue the dialogue!" ,"Listen closely... to move the captain, move the arrow keys.","To jump, press the spacebar.", "There are many ways to die so watch out. Avoid falling out of the spaceship itself!","For some reason, there are also spikes and slugs everywhere so avoid them at all costs. ","(...Why would anyone even carry spikes on the spaceship... I thought they security checked us when we got on the ship...)","Oh that's right! Occasionally, the spaceship might experience a crash and turn upside down. Be aware of a possible gravity change and falling through a crack!","Watch your oxygen meter as well! There are meters throughout the ship but if you run out of it before reaching one you'll have no way to breathe.","Now that you're ready, help us!"};

        //second set of dialogue
        string[] d2 = new string[3] { "Captain! Thank gosh you're alright! It's me, Jim!", "What's that? We need to escape? Okay, but I think more of the crew is up ahead, you can't just leave them!", "Oh right, I found another oxygen tank, take it! I'll meet you at the escape pod sir!"};

        //third set of dialogue
        string[] d3 = new string[4] { "Head engineer Jane here, a pleasure to see you again Captain!","Have you seen the size of those monsters? Who'd have thought they would get so big once they got out of their cages!","You're almost to the bridge, keep going, you're our only hope!","This oxygen container fell from the ceiling, make sure you use it!"};
        string[] d4 = new string[4] { "I can't believe you made it here captain! Navigator Kim, reporting for duty!","I got thrown out of the bridge when we got hit by that asteroid, but if you're here, we can get back in!","Use your key to get in the bridge and start the evacuation sequence, then we can all go home!","Take my O2, please! I swam in high school, I can probably hold my breath till we get out of here. Just, you know, try to be fast?"};
        string[] d5 = new string[4] {"Outfitted in his power suit, the Captain was able to repair the ships systems enough to get the escape pods going.","With seconds to spare, he jumped into the last pod, and through the small window, he could see their years of exploration go to waste. ","However, their journey sparked intrest for more expeditions, and soon, the planet Hermes was discovered to be colonizable, leading to a new front for human growth.","Though it may have ended in what could have been a trgedy, it was still quite the journey to Hermes." };
        string[] names = new string[5] { "Houston (Back on Earth)" ,"Jim","Jane","Kim","Narrator"};

        public Texture2D Menu
        {
            get { return menu; }
            set { menu = value; }
        }

        public SpriteFont DialogueText
        {
            get { return dialogueText; }
            set { dialogueText = value; }
        }

        public KeyboardState KState
        {
            get { return kState; }
            set { kState = value; }
        }

        public KeyboardState KStatePrev
        {
            get { return kStatePrev; }
            set { kStatePrev = value; }
        }

        public int DialogueCount
        {
            get { return dialogueCount; }
            set { dialogueCount = value; }
        }

        public DialogueUI()
        {
            dialogue.Add(0, d1);
            dialogue.Add(1, d2);
            dialogue.Add(2, d3);
            dialogue.Add(3, d4);
            dialogue.Add(4, d5);
        }

        //Dialogue update class
        public void Update(GameTime gameTime, KeyboardState ks, KeyboardState ksPrev)
        {
            kState = ks;
            kStatePrev = ksPrev;
            //IsDialogueDone();
            
            // dialogue for tutorial, need to implement enum(?)
            //if(gameState.DialogueTutorial)
            if (kState.IsKeyDown(Keys.Enter) && kStatePrev.IsKeyUp(Keys.Enter))
            {
                if (dialogueCount <= dialogue[dialogueToPlay].Count() -1 )
                {
                    dialogueCount += 1;
                    if(dialogueCount > dialogue[dialogueToPlay].Count() - 1)
                    {
                        dialogueCount = 0;
                        if(dialogue.ContainsKey(dialogueToPlay + 1))
                        {
                            dialogueToPlay++;
                        }
                        IsDialogueDone = true;
                        //dialogueToPlay++;
                        //gState = GameState.Playing;
                    }
                }
            }
        }

        public void Draw(SpriteBatch sB,  KeyboardState ks, KeyboardState ksPrev)
        {
            kState = ks;
            kStatePrev = ksPrev;

            sB.Draw(menu, new Vector2(0, 0), Color.White);
            sB.DrawString(dialogueText, names[dialogueToPlay], new Vector2(100, 420), Color.Black);
            //sB.DrawString(dialogueText, dialogue[0], new Vector2(100, 500), Color.Black);
            sB.DrawString(dialogueText, dialogue[dialogueToPlay][dialogueCount], new Vector2(100, 500), Color.Black);
            


        }
    }
}
