﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JourneyToHermes
{
    class Enemy
    {
        Texture2D texture;
        public Rectangle hitbox;

        Rectangle checkWall;
        Rectangle checkFloor;

        enum Orientation { gravityDown, gravityUp };
        enum Direction { faceLeft, faceRight };

        Orientation currentOrientaion;
        Direction currentDirection;

        private int gravity;
        List<Tile> mapList;

        public Enemy(Texture2D tex, int xPos, int Ypos,List<Tile> map)
        {
            texture = tex;
            hitbox = new Rectangle(xPos, Ypos, texture.Width, texture.Height);
            checkWall = hitbox;
            checkFloor = hitbox;
            currentOrientaion = Orientation.gravityDown;
            currentDirection = Direction.faceLeft;
            mapList = map;
        }

        public void Update(GameTime gameTime)
        {
            Vector2 velocity = Vector2.Zero;
            Rectangle tempY;
            Rectangle tempX;
            int speed = 2;
            //handles all the logic of what to do depending on gravity and direction
            switch (currentOrientaion)
            {
                case Orientation.gravityDown:
                    switch (currentDirection)
                    {
                        case Direction.faceLeft:
                            //facing left with gravity down
                            //update checking rectangles
                            checkFloor.X = hitbox.X - checkFloor.Width;//left
                            checkFloor.Y = hitbox.Y + checkFloor.Height;//down
                            checkWall.X = hitbox.X - checkWall.Width;//left
                            checkWall.Y = hitbox.Y;//level
                            //update gravity
                            gravity = GameProcesses.gravity;
                            if(gravity == -1)
                            {
                                currentOrientaion = Orientation.gravityUp;
                                return;
                            }
                            //check both check rectangles for intersections with the map
                            if (CollidesWithMap(checkFloor) == false)
                            {
                                currentDirection = Direction.faceRight;
                            }
                            if (CollidesWithMap(checkWall) == true)
                            {
                                currentDirection = Direction.faceRight; 
                                return;
                            }
                            //add velocity
                            velocity.X = -speed;
                            velocity.Y = speed * gravity;
                            break;
                        case Direction.faceRight:
                            //facing right with gravity down
                            //update checking rectangles
                            checkFloor.X = hitbox.X + checkFloor.Width; //right
                            checkFloor.Y = hitbox.Y + checkFloor.Height;//down
                            checkWall.X = hitbox.X + checkWall.Width;//right
                            checkWall.Y = hitbox.Y;//level
                            //update gravity
                            gravity = GameProcesses.gravity;
                            if (gravity == -1)
                            {
                                currentOrientaion = Orientation.gravityUp;
                                return;
                            }
                            //check both check rectangles for intersections with the map
                            if (CollidesWithMap(checkFloor) == false)
                            {
                                currentDirection = Direction.faceLeft;
                            }
                            if (CollidesWithMap(checkWall) == true)
                            {
                                currentDirection = Direction.faceLeft;
                                return;
                            }
                            //add velocity
                            velocity.X = speed;
                            velocity.Y = speed * gravity;
                            break;
                    }
                    break;
                case Orientation.gravityUp:
                    switch (currentDirection)
                    {
                        case Direction.faceLeft:
                            //facing left with gravity up
                            //update checking rectangles
                            checkFloor.X = hitbox.X - checkFloor.Width;//left 
                            checkFloor.Y = hitbox.Y - checkFloor.Height;//up
                            checkWall.X = hitbox.X - checkWall.Width;//left
                            checkWall.Y = hitbox.Y;//level
                            //update gravity
                            gravity = GameProcesses.gravity;
                            if (gravity == 1)
                            {
                                currentOrientaion = Orientation.gravityDown;
                                return;
                            }
                            //check both check rectangles for intersections with the map
                            if (CollidesWithMap(checkFloor) == false)
                            {
                                currentDirection = Direction.faceRight;
                            }
                            if (CollidesWithMap(checkWall) == true)
                            {
                                currentDirection = Direction.faceRight;
                                return;
                            }
                            //add velocity
                            velocity.X = -speed;
                            velocity.Y = speed * gravity;
                            break;
                        case Direction.faceRight:
                            //facing right with gravity up
                            //update checking rectangles
                            checkFloor.X = hitbox.X + checkFloor.Width;//right 
                            checkFloor.Y = hitbox.Y - checkFloor.Height;//up
                            checkWall.X = hitbox.X + checkWall.Width;//right
                            checkWall.Y = hitbox.Y;//level
                            //update gravity
                            gravity = GameProcesses.gravity;
                            if (gravity == 1)
                            {

                                currentOrientaion = Orientation.gravityDown;
                                return;
                            }
                            //check both check rectangles for intersections with the map
                            if (CollidesWithMap(checkFloor) == false)
                            {
                                currentDirection = Direction.faceLeft;
                            }
                            if(CollidesWithMap(checkWall) == true)
                            {
                                currentDirection = Direction.faceLeft;
                                return;
                            }
                            //add velocity
                            velocity.X = speed;
                            velocity.Y = speed * gravity;
                            break;
                    }
                    break;
            }
            //validate movement with collisions
            tempX = hitbox;
            tempX.X = tempX.X + (int)velocity.X;

            tempY = hitbox;
            tempY.Y = tempY.Y + (int)velocity.Y;

            //check collision
            List<Tile> xCollision = CheckTileIntersect(tempX);
            List<Tile> yCollision = CheckTileIntersect(tempY);

            //actaully move
            //X movement
            if (xCollision.Count == 0)
            {
                hitbox.X = tempX.X;
            }
            else
            {
                int tileLeft = 0;
                int tileRight = 0;
                foreach (Tile tile in xCollision)
                {
                    tileLeft = tile.Hitbox.Left;
                    tileRight = tile.Hitbox.Right;
                }

                //right
                if (velocity.X > 0)
                {
                    hitbox.X = tileLeft - 32;

                }
                //left
                if (velocity.X < 0)
                {
                    hitbox.X = tileRight;
                }
            }
            //YMovement
            if (yCollision.Count == 0)
            {
                hitbox.Y = tempY.Y;
            }
            else
            {
                int tileTop = 0;
                int tileBot = 0;
                int numDeadly = 0;
                foreach (Tile tile in yCollision)
                {
                    tileTop = tile.Hitbox.Top;
                    tileBot = tile.Hitbox.Bottom;
                    if (tile.IsDeadly)
                    {
                        numDeadly++;
                    }

                }
                //gravity pointing down
                if (currentOrientaion == Orientation.gravityDown)
                {
                    //player rising
                    if (velocity.Y < 0)
                    {
                        hitbox.Y = tileBot;
                        velocity.Y = 0;
                    }
                    //player falling
                    if (velocity.Y > 0)
                    {
                        hitbox.Y = tileTop - hitbox.Height;
                        velocity.Y = 0;
                    }
                }
                //gravity pointing up
                if (currentOrientaion == Orientation.gravityUp)
                {
                    //player falling
                    if (velocity.Y < 0)
                    {
                        hitbox.Y = tileBot;
                        velocity.Y = 0;
                    }
                    //player rising
                    if (velocity.Y > 0)
                    {
                        hitbox.Y = tileTop - hitbox.Height;
                        velocity.Y = 0;
                    }
                }
            }
        }

        public void Draw(SpriteBatch sprite)
        {
            //handles all the logic of what to do depending on gravity and direction
            switch (currentOrientaion)
            {
                case Orientation.gravityDown:
                    switch (currentDirection)
                    {
                        case Direction.faceLeft:
                            //facing left with gravity down
                            sprite.Draw(texture, hitbox, Color.White);
                            break;
                        case Direction.faceRight:
                            //facing right with gravity down
                            sprite.Draw(texture, hitbox, null, Color.White, MathHelper.PiOver2 * 4, new Vector2(0, 0), SpriteEffects.None, 0);
                            break;
                    }
                    break;
                case Orientation.gravityUp:
                    switch (currentDirection)
                    {
                        case Direction.faceLeft:
                            //facing left with gravity up
                            sprite.Draw(texture, hitbox, null, Color.White, MathHelper.PiOver2 * 2, new Vector2(32, 32), SpriteEffects.None, 0);
                            break;
                        case Direction.faceRight:
                            //facing right with gravity up
                            sprite.Draw(texture, hitbox, null, Color.White, MathHelper.Pi , new Vector2(32, 32), SpriteEffects.None, 0);
                            break;
                    }
                    break;
            }
        }

        //used to return if the givin rectangle collides with something
        public bool CollidesWithMap(Rectangle toCheck)
        {
            bool collision = false;
            foreach (Tile tile in mapList)
            {
                if (toCheck.Intersects(tile.Hitbox))
                {
                    collision = true;
                }
            }
            return collision;
        }

        //used to return data about which tiles the givin rectangle collides with
        public List<Tile> CheckTileIntersect(Rectangle toCheck)
        {
            List<Tile> tilesIntersected = new List<Tile>();
            foreach (Tile tile in mapList)
            {
                if (toCheck.Intersects(tile.Hitbox))
                {
                    tilesIntersected.Add(tile);
                }
            }
            return tilesIntersected;
        }
    }
}
