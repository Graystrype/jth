﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;


namespace JourneyToHermes
{
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Player player;
        Map map;
        List<Enemy> allEnemies;
        Song bgMusic;
        GameProcesses gameProcess;
        Camera camera;

        private int windowWidth = 1366;
        private int windowHeight = 768;
        //enumeration
        public enum GameState { StartMenu, PauseMenu, Dialogue, Playing, OptionsStartMenu, Gameover, OptionsPauseMenu,Victory }


        //start menu
        InterfaceStartMenu startMenu = new InterfaceStartMenu(new Vector2(320, 206), new Vector2(320, 366), new Vector2(320, 526));

        //pause menu
        PauseMenu pauseMenu = new PauseMenu(new Vector2(320, 206), new Vector2(320, 366), new Vector2(320, 526));

        //dialogue menu
        DialogueUI dialogueMenu = new DialogueUI();

        //gameover menu
        DeadMenu deadMenu = new DeadMenu(new Vector2(320, 281), new Vector2(320, 481));

        //options menu
        OptionsMenu optionsMenu = new OptionsMenu(new Vector2(320, 281), new Vector2(320, 481));

        //victory menu
        Victory victoryMenu = new Victory(new Vector2(320, 281), new Vector2(320, 481));

        //enum
        GameState gState;
        MouseState mStateCurrent, mStatePrev;
        KeyboardState kStateCurrent, kStatePrev;

        public GameState GState
        {
           get { return gState; }
            set { gState = value; }
        }
        //boolean for sound
        private bool sound = true;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            //1366x768 resolution
            graphics.PreferredBackBufferWidth = windowWidth;  // set this value to the desired width of your window
            graphics.PreferredBackBufferHeight = windowHeight;   // set this value to the desired height of your window
            this.Window.Position = new Point(0,0) ;
            graphics.ApplyChanges();

            //start of game
            gState = GameState.StartMenu;
        }

        protected override void Initialize()
        {
            //TODO: Add your initialization logic here
            this.IsMouseVisible = true; //makes mouse visible in game window
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            //loads the map, first parameter is the map file and the second is the texture for the tiles

            string[] fileNames = new string[2] { "map1.txt", "map2.txt" };
            Texture2D[] tileTextures = new Texture2D[9] { Content.Load<Texture2D>("tile0"), Content.Load<Texture2D>("tile1"), Content.Load<Texture2D>("tile2"), Content.Load<Texture2D>("tile3"), Content.Load<Texture2D>("tile4"),Content.Load<Texture2D>("spikes.png"),Content.Load<Texture2D>("playerTex"), Content.Load<Texture2D>("enemy"), Content.Load<Texture2D>("backgroundimage.png") };

            map = new Map(fileNames, tileTextures);
            map.CreateMap();
            allEnemies = map.SpawnEnemies();


            //load the player after the map so it can get the map passed into it
            player = new Player(Content.Load<Texture2D>("playerTex"), Content.Load<Texture2D>("O2MeterBar.png"), Content.Load<Texture2D>("O2Meter"), Content.Load<Texture2D>("O2MeterText"), Content.Load<Texture2D>("GravityChange"), Content.Load<Texture2D>("LivesText"), Content.Load<SpriteFont>("SpriteFont1"),new Vector2(850,600), map.mapTiles, map.checkpoints,this);

            //BG music custom generated from jukedeck.com
            bgMusic = Content.Load<Song>("Impossible_Streams");
            MediaPlayer.Play(bgMusic);
            MediaPlayer.IsRepeating = true;
            MediaPlayer.Pause();
            gameProcess = new GameProcesses(player);

            camera = new Camera();
            #region Start menu loads
            //loading textures for start menu object
            startMenu.Menu = Content.Load<Texture2D>("StartOverlay");
            startMenu.Button1 = Content.Load<Texture2D>("Button1");
            startMenu.Button2 = Content.Load<Texture2D>("Button2");
            startMenu.Button3 = Content.Load<Texture2D>("Button3");
            startMenu.LargeButton1 = Content.Load<Texture2D>("EnlargedButton1");
            startMenu.LargeButton2 = Content.Load<Texture2D>("EnlargedButton2");
            startMenu.LargeButton3 = Content.Load<Texture2D>("EnlargedButton3");
            #endregion

            #region Pause menu loads
            //loading textures for pause overlay object
            pauseMenu.Menu = Content.Load<Texture2D>("PauseOverlay");
            pauseMenu.Button1 = Content.Load<Texture2D>("ResumeButton1");
            pauseMenu.Button2 = Content.Load<Texture2D>("OptionsButton2");
            pauseMenu.Button3 = Content.Load<Texture2D>("BackButton3");
            pauseMenu.LargeButton1 = Content.Load<Texture2D>("EnlargedResumeButton1");
            pauseMenu.LargeButton2 = Content.Load<Texture2D>("EnlargedOptionsButton2");
            pauseMenu.LargeButton3 = Content.Load<Texture2D>("EnlargedBackButton3");
            #endregion

            #region Game over menu loads
            //loading textures for game over screen object
            deadMenu.Menu = Content.Load<Texture2D>("GameOver");
            deadMenu.Button1 = Content.Load<Texture2D>("DeadButton1");
            deadMenu.Button2 = Content.Load<Texture2D>("DeadButton2");
            deadMenu.LargeButton1 = Content.Load<Texture2D>("EnlargedDeadButton1");
            deadMenu.LargeButton2 = Content.Load<Texture2D>("EnlargedDeadButton2");
            #endregion

            #region Dialogue loads
            //loading textures for dialogue overlay
            dialogueMenu.Menu = Content.Load<Texture2D>("DialogueScreen");
            dialogueMenu.DialogueText = Content.Load<SpriteFont>("SpriteFont1");


            //loading textures for options overlay
            optionsMenu.Menu = Content.Load<Texture2D>("OptionsOverlay");
            optionsMenu.Button1 = Content.Load<Texture2D>("OptionsMute");
            optionsMenu.Button2 = Content.Load<Texture2D>("OptionsBack");
            optionsMenu.LargeButton1 = Content.Load<Texture2D>("OptionsMuteEnlarged");
            optionsMenu.LargeButton2 = Content.Load<Texture2D>("OptionsBackEnlarged");



            #endregion

            #region Victory loads
            //Victory element loading 
            victoryMenu.Menu = Content.Load<Texture2D>("Victory");
            victoryMenu.Button1 = Content.Load<Texture2D>("DeadButton1");
            victoryMenu.Button2 = Content.Load<Texture2D>("DeadButton2");
            victoryMenu.LargeButton1 = Content.Load<Texture2D>("EnlargedDeadButton1");
            victoryMenu.LargeButton2 = Content.Load<Texture2D>("EnlargedDeadButton2");
            #endregion


            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            mStateCurrent = Mouse.GetState();
            
            kStateCurrent = Keyboard.GetState();

            startMenu.MouseState = mStateCurrent;//setting mouse state for start menu
            pauseMenu.MouseState = mStateCurrent; //setting mouse state for pause menu
            deadMenu.MouseState = mStateCurrent; //setting mouse state for game over menu
            victoryMenu.MouseState = mStateCurrent; // setting mouse state for victory screen
           
            optionsMenu.MouseState = mStateCurrent; // setting mouse state for options menu
            pauseMenu.KState = kStateCurrent; //setting keyboard state for pause menu
            deadMenu.KState = kStateCurrent; // setting keyboard state for game over menu
            victoryMenu.KState = kStateCurrent; // setting keyboard state for victory screen

            #region sound
            if(sound == true && gState != GameState.Gameover)
            {
                MediaPlayer.Resume();
            }
            else
            {
                MediaPlayer.Pause();
            }
            #endregion
            #region start menu
            //start menu state
            if (gState == GameState.StartMenu)
            {
                //Console.WriteLine("Mouse Pos X: {0} Y: {1}", mStateCurrent.X, mStateCurrent.Y);
                //Console.WriteLine(this.Window.Position);
                // if mouse clicks start button then gState == GameState.Playing
                if ((startMenu.MouseState.X >= 320 && startMenu.MouseState.X <= 1064) && (startMenu.MouseState.Y >= 206 && startMenu.MouseState.Y <= 333)) 
                {
                    if (startMenu.MouseState.LeftButton == ButtonState.Pressed && startMenu.MouseStatePrev.LeftButton == ButtonState.Released)
                    {
                        player.ResetPlayer();
                        gState = GameState.Dialogue;
                    }
                }

                // if mouse clicks options button then gState == GameState.Options
                if ((startMenu.MouseState.X >= 320 && startMenu.MouseState.X <= 1064) && (startMenu.MouseState.Y >= 366 && startMenu.MouseState.Y <= 493))
                {
                    if (startMenu.MouseState.LeftButton == ButtonState.Pressed && startMenu.MouseStatePrev.LeftButton == ButtonState.Released)
                    {
                        //add options menu 
                        gState = GameState.OptionsStartMenu;
                    }
                }

                // if mouse clicks exit button then close game
                if ((startMenu.MouseState.X >= 320 && startMenu.MouseState.X <= 1064) && (startMenu.MouseState.Y >= 526 && startMenu.MouseState.Y <= 653))
                {
                    if (startMenu.MouseState.LeftButton == ButtonState.Pressed && startMenu.MouseStatePrev.LeftButton == ButtonState.Released)
                    {
                        Exit();
                    }                
                }
            }
            #endregion

            #region playing
            //PLAYING STATE
            if (gState == GameState.Playing)
            {
                if (player.hasWon)
                {
                    gState = GameState.Victory;
                }
                foreach(Enemy e in allEnemies)
                {
                    e.Update(gameTime);
                    if (player.playerHitbox.Intersects(e.hitbox))
                    {
                        player.IsAlive = false;
                    }
                }

                player.Update(gameTime);
                if (player.shouldDialogue)
                {
                    gState = GameState.Dialogue;
                    player.shouldDialogue = false;
                }
                gameProcess.Update(gameTime);

                camera.Move(new Vector2(player.playerHitbox.X, camera.position.Y));

                if (kStateCurrent.IsKeyDown(Keys.Escape))
                {
                    gState = GameState.PauseMenu;
                }

                if (player.IsAlive == false)
                {
                    if(player.NumLives <= 0)
                    {
                        gState = GameState.Gameover;
                        player.ResetPlayer();
                    }
                    else
                    {
                        player.ResetAtCheckpoint();
                        gameProcess.Timer = 0;
                    }
                }               
            }
            #endregion

            #region dialogue
            //DIALOGUE STATE
            if (gState == GameState.Dialogue)
            {
                dialogueMenu.Update(gameTime, kStateCurrent, kStatePrev);

                //if click enter, go to the next string/text (right now all the dialogueui text is hard coded in as strings)
                if (dialogueMenu.IsDialogueDone == true)
                {
                    dialogueMenu.IsDialogueDone = false;
                    gState = GameState.Playing;
                }
                
            }
            #endregion

            #region gameover
            //GAMEOVER STATE
            if (gState == GameState.Gameover)
            {
                //retry goes to playing state
                if ((deadMenu.MouseState.X >= 320 && deadMenu.MouseState.X <= 1064) && (deadMenu.MouseState.Y >= 281 && deadMenu.MouseState.Y <= 396))
                {
                    if (deadMenu.MouseState.LeftButton == ButtonState.Pressed && deadMenu.MouseStatePrev.LeftButton == ButtonState.Released)
                    {
                        player.ResetPlayer();
                        gState = GameState.Playing;
                    }
                    
                }
                //quit goes to start menu
                if ((deadMenu.MouseState.X >= 320 && deadMenu.MouseState.X <= 1064) && (deadMenu.MouseState.Y >= 481 && deadMenu.MouseState.Y <= 596))
                {
                    if (deadMenu.MouseState.LeftButton == ButtonState.Pressed && deadMenu.MouseStatePrev.LeftButton == ButtonState.Released)
                    {
                        gState = GameState.StartMenu;
                    }
                }
            }
            #endregion

            #region victory
            //VICTORY STATE
            if (gState == GameState.Victory)
            {
                //retry goes to playing state
                if ((victoryMenu.MouseState.X >= 320 && victoryMenu.MouseState.X <= 1064) && (victoryMenu.MouseState.Y >= 281 && victoryMenu.MouseState.Y <= 396))
                {
                    if (victoryMenu.MouseState.LeftButton == ButtonState.Pressed && victoryMenu.MouseStatePrev.LeftButton == ButtonState.Released)
                    {
                        player.ResetPlayer();
                        gState = GameState.Playing;
                    }

                }
                //quit goes to start menu
                if ((victoryMenu.MouseState.X >= 320 && victoryMenu.MouseState.X <= 1064) && (victoryMenu.MouseState.Y >= 481 && victoryMenu.MouseState.Y <= 596))
                {
                    if (victoryMenu.MouseState.LeftButton == ButtonState.Pressed && victoryMenu.MouseStatePrev.LeftButton == ButtonState.Released)
                    {
                        gState = GameState.StartMenu;
                    }
                }
            }
            #endregion

            #region pause
            //PAUSE MENU STATE
            if (gState == GameState.PauseMenu)
            {
                //click resume, go back to playing gamestate
                if ((pauseMenu.MouseState.X >= 320 && pauseMenu.MouseState.X <= 1064) && (pauseMenu.MouseState.Y >= 206 && pauseMenu.MouseState.Y <= 333))
                {
                    if (pauseMenu.MouseState.LeftButton == ButtonState.Pressed && pauseMenu.MouseStatePrev.LeftButton == ButtonState.Released)
                    {
                        gState = GameState.Playing;
                    }
                }

                //click options, go to options gamestate
                if ((pauseMenu.MouseState.X >= 320 && pauseMenu.MouseState.X <= 1064) && (pauseMenu.MouseState.Y >= 366 && pauseMenu.MouseState.Y <= 493))
                {
                    //option settings
                    if (pauseMenu.MouseState.LeftButton == ButtonState.Pressed && pauseMenu.MouseStatePrev.LeftButton == ButtonState.Released)
                    {
                        gState = GameState.OptionsPauseMenu; 
                    }
                    
                }

                //click exit button, leave gameplay end game  
                if ((pauseMenu.MouseState.X >= 320 && pauseMenu.MouseState.X <= 1064) && (pauseMenu.MouseState.Y >= 526 && pauseMenu.MouseState.Y <= 653))
                {
                        if (pauseMenu.MouseState.LeftButton == ButtonState.Pressed && pauseMenu.MouseStatePrev.LeftButton == ButtonState.Released)
                        {
                            gState = GameState.StartMenu;
                        player.ResetPlayer();
                        gameProcess.Timer = 0;
                        }      
                }
            }
            #endregion

            #region start options
            //OPTIONS StartMenu STATE
            if (gState == GameState.OptionsStartMenu)
            {
                //Mute button
                if ((optionsMenu.MouseState.X >= 320 && optionsMenu.MouseState.X <= 1064) && (optionsMenu.MouseState.Y >= 281 && optionsMenu.MouseState.Y <= 396))
                {
                    if (optionsMenu.MouseState.LeftButton == ButtonState.Pressed && optionsMenu.MouseStatePrev.LeftButton == ButtonState.Released)
                    {
                        //boolean we'll use later for sound
                        if (sound == true)
                        {
                            sound = false;
                            Console.WriteLine(sound);
                        }
                        else
                        {
                            sound = true;
                            Console.WriteLine(sound);
                        }
                    }
                }

                //Back goes to pause screen
                if ((optionsMenu.MouseState.X >= 320 && optionsMenu.MouseState.X <= 1064) && (optionsMenu.MouseState.Y >= 481 && optionsMenu.MouseState.Y <= 596))
                {
                    if (optionsMenu.MouseState.LeftButton == ButtonState.Pressed && optionsMenu.MouseStatePrev.LeftButton == ButtonState.Released)
                    {
                        gState = GameState.StartMenu;
                    }
                }
            }
            #endregion

            #region pause options

            //OPTIONS PauseMenu STATE
            if (gState == GameState.OptionsPauseMenu)
            {
                //Mute button
                if ((optionsMenu.MouseState.X >= 320 && optionsMenu.MouseState.X <= 1064) && (optionsMenu.MouseState.Y >= 281 && optionsMenu.MouseState.Y <= 396))
                {
                    if (optionsMenu.MouseState.LeftButton == ButtonState.Pressed && optionsMenu.MouseStatePrev.LeftButton == ButtonState.Released)
                    {
                        //boolean we'll use later for sound
                        if (sound == true)
                        {
                            sound = false;

                        }
                        else
                        {
                            sound = true;

                        }
                    }
                }

                //Back goes to pause screen
                if ((optionsMenu.MouseState.X >= 320 && optionsMenu.MouseState.X <= 1064) && (optionsMenu.MouseState.Y >= 481 && optionsMenu.MouseState.Y <= 596))
                {
                    if (optionsMenu.MouseState.LeftButton == ButtonState.Pressed && optionsMenu.MouseStatePrev.LeftButton == ButtonState.Released)
                    {
                        gState = GameState.PauseMenu;
                    }
                }
            }
            #endregion

            #region victory screen
            if (gState == GameState.Victory)
            {
                player.ResetPlayer();
                gState = GameState.Victory;
            }
            #endregion
            mStatePrev = mStateCurrent;
            kStatePrev = kStateCurrent;



            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.LightGray);
            //Console.WriteLine(graphics.GraphicsDevice.Viewport.Width);
            //decide which state the game is in and draw the appropriate menu or objects
            if (gState == GameState.Playing)
            {
                spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, camera.GetNewTransform(GraphicsDevice));
                map.Draw(spriteBatch);
                
                foreach (Enemy e in allEnemies)
                {
                    e.Draw(spriteBatch);
                }
                player.Draw(spriteBatch);
                spriteBatch.End();
            }
            else
            {
                spriteBatch.Begin();
                if (gState == GameState.StartMenu)
                {
                    startMenu.Draw(spriteBatch, mStateCurrent, mStatePrev);
                }

                if (gState == GameState.PauseMenu)
                {
                    pauseMenu.Draw(spriteBatch, mStateCurrent, mStatePrev);
                }

                if (gState == GameState.OptionsStartMenu)
                {
                    //add options menu here
                    optionsMenu.Draw(spriteBatch, mStateCurrent, mStatePrev);
                }

                if (gState == GameState.OptionsPauseMenu)
                {
                    //add options menu here
                    optionsMenu.Draw(spriteBatch, mStateCurrent, mStatePrev);
                }

                if (gState == GameState.Gameover)
                {
                    deadMenu.Draw(spriteBatch);
                }

                if (gState == GameState.Dialogue)
                {
                    dialogueMenu.Draw(spriteBatch, kStateCurrent, kStatePrev);
                }

                if (gState == GameState.Victory)
                {
                    victoryMenu.Draw(spriteBatch);
                }
                spriteBatch.End();
            }

            base.Draw(gameTime);
        }
    }
}
