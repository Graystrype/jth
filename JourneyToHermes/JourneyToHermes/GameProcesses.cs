﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
/*
 * Malcolm Lambrecht game processes class
 * For now just controls gravity switching, later will control checkpoints 
 * Add a sligtly random element to the timer
 */
 
namespace JourneyToHermes
{
    class GameProcesses
    {
        Player player;
        public double timeToSwitch = 7; //seconds between gravity switches
        public double timer = 0;
        Random rng = new Random();
        public static int gravity = 1;
        private int minTime = 7;
        private int maxTime = 11;


        public GameProcesses(Player player)
        {
            this.player = player;
        }

        public double Timer
        {
            get { return timer; }
            set { timer = value; }
        }


        public void Update(GameTime gameTime)
        {
            timer += gameTime.ElapsedGameTime.TotalMilliseconds; // Increment the timer by the elapsed game time.

            if (timer >= timeToSwitch * 1000) // Check to see if one second has passed.
            {
                timer = 0;
                player.isWarned = false;
                FlipGravity();

                timeToSwitch = rng.Next(minTime, maxTime);

            }

            if (timer >= (timeToSwitch - 2.5) * 1000) // Check to see if time is close to switching
            {
                player.isWarned = true;
            }
            else
            {
                player.isWarned = false;
            }
        }

        //method that flips gravity
        public void FlipGravity()
        {
            gravity = gravity * -1;
        }
    }
}
