﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.IO;
using System.Reflection;
/*
* Malcolm Lambrecht, Map Class Journey to Hermes
* Reads a text file and creates the map based on that
* works pretty well for tiles, havent added functionality for spikes, enemies, or checkpoints
* needs some commenting
* EVERY TILE NEEDS ANOTHER TILE ADJACENT TO IT SPIKES DO NOT COUNT AS TILES
*/
namespace JourneyToHermes
{
    class Map
    {
        //used by constructor
        //array of possible textures the tiles could have
        Texture2D[] tileTexArray;
        //0 = no borders
        //1 = 1 edge
        //2 = edges on opposite sides
        //3 = corner
        //4 = endcap
        //5 = spikes
        //6 = checkpoint
        //7 = enemy
        //8 bg image
        string[] fileNames;
        //in the text file being read in
            //0 = empty
            //1 = tile
            //2 = spike
            //3 = checkpoint
            //4 = place to spawn an enemy

        //constants about size
        const int TILE_WIDTH = 32; //size of a tile in pixels, need to get this in a better way than hardcoding
        const int MAP_HEIGHT = 24; //number of rows in the map
        int mapLength; //number of columns in the map set when the files are read in int the ReadFiles method

        //list of strings created after the files are read in, each array in the list is a row
        List<string[]> processedRowStrings;
        List<List<int>> mapValues;
        public List<Tile> mapTiles;
        public List<Tile> checkpoints;

        //contructor for the map class, sets the file name for the reader to take
        public Map(string[] mapFileNames, Texture2D[] tileTextures)
        {
            fileNames = new string[mapFileNames.Length];
            for(int i = 0; i < mapFileNames.Length; i++)
            {
                fileNames[i] = mapFileNames[i];
            }
            tileTexArray = new Texture2D[tileTextures.Length];
            for (int i = 0; i < tileTextures.Length; i++)
            {
                tileTexArray[i] = tileTextures[i];
            }
        }

        //creates the map by calling all of the other methods in the right order
        public void CreateMap()
        {
            //need method to read in files
            ReadFiles(); // works correctly

            //need method to process string to ints
            ProcessStrings(); //works correctly

            //need method to process ints into tiles
            checkpoints = new List<Tile>();
            CreateTileMap();//works correctly
        }
 
        //read in the map files to an array of strings
        public void ReadFiles()
        {
            //final array after everything is read in
            string[] rowStringMaster = new string[MAP_HEIGHT];
            processedRowStrings = new List<string[]>(); 

            foreach(string fileName in fileNames)
            {
                //gets an array of each line of the file
                string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"Content\" + fileName);
                string[] rows = File.ReadAllLines(path);

                //add the new row extensions to the master rows list
                for(int i = 0; i < rowStringMaster.Length; i++)
                {
                    rowStringMaster[i] = rowStringMaster[i] + rows[i];
                }
            }

            //process the string that represents an entire row into an array of strings that represent each tile
            foreach(string row in rowStringMaster)
            {
                string[] splitRow = row.Split();
                mapLength = splitRow.Length;
                processedRowStrings.Add(splitRow);
            }           
        }

        //process the read in data into ints
        public void ProcessStrings()
        {
            mapValues = new List<List<int>>();
            //do this for each row
            foreach(string[] stringRow in processedRowStrings)
            {
                List<int> tempRow = new List<int>();

                //do this for each string in the row
                foreach(string stringValue in stringRow)
                {
                    int tempValue = -1;
                    int.TryParse(stringValue, out tempValue);
                    tempRow.Add(tempValue);
                }

                mapValues.Add(tempRow);
            }
        }

        //create the list of tiles
        public void CreateTileMap()
        {
            mapTiles = new List<Tile>();

            //iterates through all the map values
            for(int row = 0; row < mapValues.Count; row++)
            {
                List<int> tempRow = mapValues[row];

                for(int col = 0; col < tempRow.Count; col++)
                {
                    //do this for every value in the map data
                    //if its a normal tile
                    if (tempRow[col] == 1)
                    {

                        Tile tempTile = new Tile(col,row);
                        AssignTexture(tempTile);
                        tempTile.Hitbox = new Rectangle(tempTile.XCoord * TILE_WIDTH, tempTile.YCoord * TILE_WIDTH, tempTile.Texture.Width, tempTile.Texture.Height);
                        mapTiles.Add(tempTile);
                    }
                    //if its a spike
                    if(tempRow[col] == 2)
                    {
                        Tile tempTile = new Tile(col, row);
                        tempTile.IsDeadly = true;
                        AssignTexture(tempTile);                       
                        tempTile.Hitbox = new Rectangle(tempTile.XCoord * TILE_WIDTH, tempTile.YCoord * TILE_WIDTH, tempTile.Texture.Width, tempTile.Texture.Height);
                        mapTiles.Add(tempTile);
                    }
                    //checkpoints
                    if(tempRow[col] == 3)
                    {
                        int dNum = 1;
                        Tile cPoint = new Tile(col, row);
                        cPoint.Texture = tileTexArray[6];
                        cPoint.Hitbox = new Rectangle(cPoint.XCoord * TILE_WIDTH, (cPoint.YCoord * TILE_WIDTH) - 32, cPoint.Texture.Width, cPoint.Texture.Height);
                        cPoint.dialogueNum = dNum;
                        dNum++;
                        cPoint.firstTouch = true;
                        checkpoints.Add(cPoint);
                        //reset the final checkpoint marker
                        
                    }
                }
            }
            checkpoints[checkpoints.Count - 1].IsFinalCheckpoint = true;
        }

        public void AssignTexture(Tile tile)
        {
            //values to store data about its neighboring tiles
            bool north = false;
            bool east = false;
            bool south = false;
            bool west = false;
            int numNeighbors = 0;

            List<int> tempRow;

            //create a point one tile in every direction
            Point northP = new Point(tile.XCoord, tile.YCoord - 1);
            Point eastP = new Point(tile.XCoord + 1, tile.YCoord);
            Point southP = new Point(tile.XCoord, tile.YCoord + 1);
            Point westP = new Point(tile.XCoord - 1, tile.YCoord);

            //validate each point, count it as full if its invalid
            if(northP.Y < 0)
            {
                north = true;
                numNeighbors++;
            }
            if (eastP.X >= mapLength)
            {
                east = true;
                numNeighbors++;
            }
            if (southP.Y > MAP_HEIGHT - 1)
            {
                south = true;
                numNeighbors++;
            }
            if (westP.X < 0)
            {
                west = true;
                numNeighbors++;
            }
            //check north
            if(north == false)
            {
                tempRow = mapValues[northP.Y];
                if(tempRow[northP.X] == 1)
                {
                    north = true;
                    numNeighbors++;
                }
            }
            //check east
            if (east == false)
            {
                tempRow = mapValues[eastP.Y];
                if (tempRow[eastP.X] == 1)
                {
                    east = true;
                    numNeighbors++;
                }
            }
            //check south
            if (south == false)
            {
                tempRow = mapValues[southP.Y];
                if (tempRow[southP.X] == 1)
                {
                    south = true;
                    numNeighbors++;
                }
            }
            //check west
            if (west == false)
            {
                tempRow = mapValues[westP.Y];
                if (tempRow[westP.X] == 1)
                {
                    west = true;
                    numNeighbors++;
                }
            }
            //assign a texture and rotaion value for the tile base on the neighbors
            //normal tile
            if (tile.IsDeadly == false)
            {
                switch (numNeighbors)
                {
                    case 1:
                        tile.Texture = tileTexArray[4];
                        if (north == true)
                        {
                            tile.RotateBy = 270;
                        }
                        else if (east == true)
                        {
                            tile.RotateBy = 0;
                        }
                        else if (south == true)
                        {
                            tile.RotateBy = 90;
                        }
                        else if (west == true)
                        {
                            tile.RotateBy = 180;
                        }
                        break;
                    case 2:
                        //opposite neighbors
                        if (north == true && south == true)
                        {
                            tile.Texture = tileTexArray[2];
                            tile.RotateBy = 90;
                        }
                        else if (east == true && west == true)
                        {
                            tile.Texture = tileTexArray[2];
                            tile.RotateBy = 0;
                        }
                        //adjacent neighbors
                        if (north == true && east == true)
                        {
                            tile.Texture = tileTexArray[3];
                            tile.RotateBy = 270;
                        }
                        else if (east == true && south == true)
                        {
                            tile.Texture = tileTexArray[3];
                            tile.RotateBy = 0;
                        }
                        else if (south == true && west == true)
                        {
                            tile.Texture = tileTexArray[3];
                            tile.RotateBy = 90;
                        }
                        else if (west == true && north == true)
                        {
                            tile.Texture = tileTexArray[3];
                            tile.RotateBy = 180;
                        }
                        break;
                    case 3:
                        tile.Texture = tileTexArray[1];
                        if (north == false)
                        {
                            tile.RotateBy = 0;
                        }
                        else if (east == false)
                        {
                            tile.RotateBy = 90;
                        }
                        else if (south == false)
                        {
                            tile.RotateBy = 180;
                        }
                        else if (west == false)
                        {
                            tile.RotateBy = 270;
                        }
                        break;
                    case 4:
                        tile.Texture = tileTexArray[0];
                        tile.RotateBy = 0;
                        break;
                }
            }

            #region Spike switch
            //its a spike
            if(tile.IsDeadly == true)
            {
                tile.Texture = tileTexArray[5];

                switch (numNeighbors)
                {
                    case 1:
                        if (north == true)
                        {
                            tile.RotateBy = 180;
                        }
                        else if (east == true)
                        {
                            tile.RotateBy = 270;
                        }
                        else if (south == true)
                        {
                            tile.RotateBy = 0;
                        }
                        else if (west == true)
                        {
                            tile.RotateBy = 90;
                        }
                        break;
                    case 2:
                        if(north == true)
                        {
                            tile.RotateBy = 180;
                        }
                        else if(south == true)
                        {
                            tile.RotateBy = 0;
                        }
                        else
                        {
                            tile.RotateBy = 270;
                        }
                        break;
                    case 3:
                        if (north == false)
                        {
                            tile.RotateBy = 0;
                        }
                        else if (east == false)
                        {
                            tile.RotateBy = 90;
                        }
                        else if (south == false)
                        {
                            tile.RotateBy = 180;
                        }
                        else if (west == false)
                        {
                            tile.RotateBy = 270;
                        }
                        break;
                    default:
                        tile.RotateBy = 0;
                        break;
                }
            }
            #endregion 
        }

        //make return a list of enemies
        public List<Enemy> SpawnEnemies()
        {
            List<Enemy> enemies = new List<Enemy>();
            for (int row = 0; row < mapValues.Count; row++)
            {
                List<int> tempRow = mapValues[row];

                for (int col = 0; col < tempRow.Count; col++)
                {
                    if (tempRow[col] == 4)
                    {
                        enemies.Add(new Enemy(tileTexArray[7], col * 32, row * 32, mapTiles));
                    }
                }
            }
            return enemies;
        }


        
        //draw tiles, rotating them the proper amount
        public void Draw(SpriteBatch spritebatch)
        {
            //draws the bg image on a loop increse num of iterations to extend bg
            for (int i = 0; i < 10; i++)
            {
                spritebatch.Draw(tileTexArray[8], new Rectangle(tileTexArray[8].Width * i, 0, tileTexArray[8].Width, tileTexArray[8].Height), Color.White);

            }
            foreach (Tile tile in mapTiles)
            {
                if(tile.RotateBy == 0)
                {
                    spritebatch.Draw(tile.Texture, tile.Hitbox, Color.White);
                }
                if (tile.RotateBy == 90)
                {
                    spritebatch.Draw(tile.Texture, tile.Hitbox, null, Color.White, MathHelper.PiOver2, new Vector2(0, TILE_WIDTH), SpriteEffects.None, 0);
                }
                if (tile.RotateBy == 180)
                {
                    spritebatch.Draw(tile.Texture, tile.Hitbox, null, Color.White, MathHelper.Pi, new Vector2(TILE_WIDTH, TILE_WIDTH), SpriteEffects.None, 0);
                }
                if (tile.RotateBy == 270)
                {
                    spritebatch.Draw(tile.Texture, tile.Hitbox, null, Color.White, MathHelper.PiOver2 * 3, new Vector2(TILE_WIDTH, 0), SpriteEffects.None, 0);
                }
            } 
            foreach(Tile cp in checkpoints)
            {
                spritebatch.Draw(cp.Texture, cp.Hitbox, Color.CadetBlue);
            }
                    
        }
    }
}
