﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input; //needed for XNA

namespace JourneyToHermes
{
    class OptionsMenu
    {
        //Attributes for game over overlay
        Texture2D menu;
        Texture2D button1; // 744 x 115
        Texture2D button2;
        Texture2D largeButton1; // 818 x 127
        Texture2D largeButton2;
        Vector2 pos1;
        Vector2 pos2;
        MouseState mouseState;
        MouseState mouseStatePrev;
        KeyboardState kState;

        //properties for the attributes
        public Texture2D Menu
        {
            get { return menu; }
            set { menu = value; }
        }

        public Texture2D Button1
        {
            get { return button1; }
            set { button1 = value; }
        }

        public Texture2D Button2
        {
            get { return button2; }
            set { button2 = value; }
        }

        public Texture2D LargeButton1
        {
            get { return largeButton1; }
            set { largeButton1 = value; }
        }

        public Texture2D LargeButton2
        {
            get { return largeButton2; }
            set { largeButton2 = value; }
        }

        public Vector2 Pos1
        {
            get { return pos1; }
            set { pos1 = value; }
        }

        public Vector2 Pos2
        {
            get { return pos2; }
            set { pos2 = value; }
        }

        public MouseState MouseState
        {
            get { return mouseState; }
            set { mouseState = value; }
        }
        public MouseState MouseStatePrev
        {
            get { return mouseStatePrev; }
            set { mouseStatePrev = value; }
        }
        public KeyboardState KState
        {
            get { return kState; }
            set { kState = value; }
        }

        //Constructor
        public OptionsMenu(Vector2 position1, Vector2 position2)
        {
            //attributes = to parameters taken in

            pos1 = position1;
            pos2 = position2;
        }

        public void Draw(SpriteBatch sB, MouseState ms, MouseState msPrev)
        {
            mouseState = ms;
            mouseStatePrev = msPrev;

            sB.Draw(menu, new Vector2(0, 0), Color.White);
            sB.Draw(button1, pos1, Color.White);
            sB.Draw(button2, pos2, Color.White);

            //button1 enlarge
            if ((mouseState.X >= 320 && mouseState.X <= 1064) && (mouseState.Y >= 281 && mouseState.Y <= 396)) //Old pos.y + enlarged rectangle width
            {
                sB.Draw(largeButton1, new Vector2(pos1.X - 30, pos1.Y), Color.White);
            }

            //button2 enlarge
            if ((mouseState.X >= 320 && mouseState.X <= 1064) && (mouseState.Y >= 481 && mouseState.Y <= 596))
            {
                sB.Draw(largeButton2, new Vector2(pos2.X - 30, pos2.Y), Color.White);
            }

            mouseStatePrev = mouseState;

        }
    }
}
