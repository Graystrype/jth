﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

/*
 * Malcolm Lambrecht, Player Class, Journey to Hermes
 * Handles player movement and collisions
 */

namespace JourneyToHermes
{
    // left and right arrows control left and right, space is jump
    class Player
    {
        private int gravity; //should be either 1 or -1
        bool hasJumped; //keeps track of if the player has jumped
        List<Tile> mapList; // the list of map objects a player can collide with
        List<Tile> cpList; //list of checkpoints

        KeyboardState keyS = new KeyboardState();
        KeyboardState prevS = new KeyboardState();

        //player attributes
        public Rectangle playerHitbox;
        public Texture2D texture;
        public Texture2D oxygenMeter;
        public Texture2D oxygenContainer;
        public Texture2D oxygenText;
        public Texture2D gravityWarning;
        public Texture2D livesText;
        public SpriteFont lives;

        Vector2 velocity;
        public Vector2 spawnPos;
        public Vector2 lastCheckpoint;

        private bool isAlive = true;
        public bool isWarned = false;
        public bool hasWon = false;
        int numLives = 5;
        int maxLives = 5;
        public bool shouldDialogue = false;

        //player movement characteristics
        private float speedScale = 0.5f;//controls how fast he palyer speeds up and slows down
        private float moveSpeed = 3f; //the players horizontal movement speed       
        private float jumpSpeed = 9f; //velocity added when jumps
        private float gravityConst = 0.27f; //higher numbers mean gravity applies faster

        public float currentOxygen = 45; //number of seconds the player will have until gameover due to lack of oxygen
        public float maxOxygen = 45;
        bool canMove = true;
        int disableFor = 0;
        Game1 g;

        //accessor for oxygen meter

        public int CurrentOxygen
        {
            get { return (int)currentOxygen; }
        }

        private double timer = 0;

        //used for collision smoothing (prevents weird gaps) should be the number of pixels to a side of a tile
        const int TILE_SIZE = 32;

        public bool IsAlive
        {
            get { return isAlive; }
            set { isAlive = value; }
        }

        public int NumLives
        {
            get { return numLives; }
        }

        //creates a new player object
        public Player(Texture2D tex, Texture2D oxygen, Texture2D oxygenBorder, Texture2D oxygenTxt, Texture2D gravitySign, Texture2D life, SpriteFont lifeFont, Vector2 pos, List<Tile> map, List<Tile> checkpoints, Game1 game)
        {
            texture = tex;
            oxygenMeter = oxygen;
            oxygenContainer = oxygenBorder;
            oxygenText = oxygenTxt;
            gravityWarning = gravitySign;
            livesText = life;
            lives = lifeFont;
            spawnPos = pos;
            g = game;
            playerHitbox.X = (int)pos.X;
            playerHitbox.Y = (int)pos.Y;
            playerHitbox.Width = tex.Width;
            playerHitbox.Height = tex.Height;

            hasJumped = true;
            mapList = map;
            cpList = checkpoints;
            lastCheckpoint = spawnPos;
        }

        //handles the player movement and collsions
        public void Update(GameTime gameTime)
        {
            //update oxygen timer
            PlayerTimer(gameTime);
            gravity = GameProcesses.gravity;

            //check if has fallen off of map
            if(this.playerHitbox.Y < -64 * 2|| this.playerHitbox.Y > 768 + (64 * 2))
            {
                this.isAlive = false;
                
            }
            //gets the keyboard state for control purposes
            keyS = Keyboard.GetState();

            //allows for disabling controls temporarily on death
            if(canMove == true)
            {
                //check for horizontal movement keys and change the x velocity 
                if (keyS.IsKeyDown(Keys.Left) == true)
                {
                    //left is negative
                    //add the max speed times the speed scale to the velocity
                    velocity.X += -moveSpeed * speedScale;
                    //clamp speed to the max speed
                    if (velocity.X < -moveSpeed)
                    {
                        velocity.X = -moveSpeed;
                    }
                }
                else if (keyS.IsKeyDown(Keys.Right) == true)
                {
                    //right is positive
                    //add the max speed times the speed scale to the velocity
                    velocity.X += moveSpeed * speedScale;
                    //clamp speed to the max speed
                    if (velocity.X > moveSpeed)
                    {
                        velocity.X = moveSpeed;
                    }
                }
                else
                {
                    //multiplying by a higher number makes the character feel more slippery, over one means they never stop without hitting something
                    velocity.X = velocity.X * (1 - speedScale);
                }

                //vertical movement
                //checks if the player is allowed to jump and the jump key is down
                if (keyS.IsKeyDown(Keys.Space) == true && hasJumped == false && prevS.IsKeyUp(Keys.Space))
                {
                    //give the player a Y velocity into the air
                    velocity.Y = -jumpSpeed * gravity;

                    //tells the game the player cant jump again until they land
                    hasJumped = true;
                }
            }
            

            //apply gravity no matter what
            velocity.Y += gravityConst * gravity;

            //BEGIN COLLISION CODE
            //create a new temp rectangles identical to the player
            //creates two to check horizontal and vertical motion seperately
            Rectangle newXPos = new Rectangle(playerHitbox.X + (int)velocity.X, playerHitbox.Y, playerHitbox.Width, playerHitbox.Height);
            Rectangle newYPos = new Rectangle(playerHitbox.X, playerHitbox.Y + (int)velocity.Y, playerHitbox.Width, playerHitbox.Height);

            //check collision
            List<Tile> xCollision = CheckTileIntersect(newXPos);
            List<Tile> yCollision = CheckTileIntersect(newYPos);

            //collision smoothing
            //check if there were any collisions, if there werent any collisions set the players pos = to new pos
            if(xCollision.Count == 0)
            {
                playerHitbox.X = newXPos.X;
            }
            else
            {
                int tileLeft = 0;
                int tileRight = 0;
                int numDeadly = 0;
                //check if they were fatal tiles
                foreach (Tile tile in xCollision)
                {
                    tileLeft = tile.Hitbox.Left;
                    tileRight = tile.Hitbox.Right;
                    
                    if (tile.IsDeadly == true)
                    {
                        numDeadly++;
                    }
                }
                if (numDeadly == xCollision.Count)
                {
                    isAlive = false;
                }
                //right
                if (velocity.X > 0)
                {
                    playerHitbox.X = tileLeft - TILE_SIZE;

                }
                //left
                if (velocity.X < 0)
                {
                    playerHitbox.X = tileRight;
                }
            }

            //y collision smoothing
            if(yCollision.Count == 0)
            {
                playerHitbox.Y = newYPos.Y;
            }
            else
            {
                int tileTop = 0;
                int tileBot = 0;
                int numDeadly = 0;
                foreach (Tile tile in yCollision)
                {
                    tileTop = tile.Hitbox.Top;
                    tileBot = tile.Hitbox.Bottom;
                    if (tile.IsDeadly)
                    {
                        numDeadly++;
                    }
                                      
                }
                //prevents some wierd collisions when youre standing on a safe tile and hanging over the edge of a spike
                if (numDeadly == yCollision.Count)
                {
                    isAlive = false;
                }
                
                //gravity pointing down
                if(gravity == 1)
                {
                    //player rising
                    if(velocity.Y < 0)
                    {
                        playerHitbox.Y = tileBot;
                        velocity.Y = 0;
                    }
                    //player falling
                    if (velocity.Y > 0)
                    {
                        playerHitbox.Y = tileTop - playerHitbox.Height;
                        hasJumped = false;
                        velocity.Y = 0;
                    }
                }
                //gravity pointing up
                if (gravity == -1)
                {
                    //player falling
                    if (velocity.Y < 0)
                    {
                        playerHitbox.Y = tileBot;
                        hasJumped = false;
                        velocity.Y = 0;
                    }
                    //player rising
                    if (velocity.Y > 0)
                    {
                        playerHitbox.Y = tileTop - playerHitbox.Height;
                        velocity.Y = 0;
                    }
                }
            }
            CheckForCheckpoint(playerHitbox);
            prevS = keyS;
        }

        //called during update to keep track of the oxygen timer
        public void PlayerTimer(GameTime gameTime)
        {
            timer += gameTime.ElapsedGameTime.TotalMilliseconds; // Increment the timer by the elapsed game time.

            if (timer >= 1000) // Check to see if one second has passed.
            {
                //the game class will check the value of oxygen and switch states based on that
                currentOxygen--;
                timer -= 1000; // Reset the timer.
                if(currentOxygen == 0)
                {
                    isAlive = false;
                }
                if(canMove == false)
                {
                    disableFor--;
                    if (disableFor <= 0)
                    {
                        canMove = true;
                    }
                }
            }

        }

        //used to return data about which tiles
        public List<Tile> CheckTileIntersect(Rectangle playerHitbox)
        {
            List<Tile> tilesIntersected = new List<Tile>();
            foreach (Tile tile in mapList)
            {
                if (playerHitbox.Intersects(tile.Hitbox))
                {
                    tilesIntersected.Add(tile);
                }    
            }
            return tilesIntersected;
        }

        //checks for collisions with a checkpoint
        public void CheckForCheckpoint(Rectangle playerHitbox)
        {
            foreach(Tile cp in cpList)
            {
                if (playerHitbox.Intersects(cp.Hitbox))
                {
                    currentOxygen = maxOxygen;                    
                    if (cp.Hitbox.X > lastCheckpoint.X)
                    {
                        lastCheckpoint.X = cp.Hitbox.X;
                        lastCheckpoint.Y = playerHitbox.Y;
                    }
                    if(cp.IsFinalCheckpoint == true)
                    {
                        hasWon = true;
                    }
                    if(cp.firstTouch == true)
                    {
                        cp.firstTouch = false;

                        shouldDialogue = true;
                    }
                }
            }
        }

        //used to fully reset the player to the beginning of the level
        public void ResetPlayer()
        {
            playerHitbox.X = (int)spawnPos.X;
            playerHitbox.Y = (int)spawnPos.Y;
            lastCheckpoint = spawnPos;
            currentOxygen = maxOxygen;
            velocity = Vector2.Zero;
            isAlive = true;
            hasJumped = true;
            numLives = maxLives;
            hasWon = false;
            //Resetting gravity when you respawn
            GameProcesses.gravity = 1;
        }

        //used to reset the player to the last checkpoint
        public void ResetAtCheckpoint()
        {
            playerHitbox.X = (int)lastCheckpoint.X;
            playerHitbox.Y = (int)lastCheckpoint.Y;
            currentOxygen = maxOxygen;
            velocity = Vector2.Zero;
            isAlive = true;
            hasJumped = true;
            numLives--;
            canMove = false;
            disableFor = 1;
        }

        //draw the player
        public void Draw(SpriteBatch sprite)
        {
            
            //different colors depending on gravity for testing
            switch (gravity)
            {
                case 1:
                    sprite.Draw(texture, playerHitbox, Color.White);
                    break;
                case -1:
                    //public void Draw(Texture2D texture, Rectangle destinationRectangle, Rectangle? sourceRectangle, Color color, float rotation, Vector2 origin, SpriteEffects effects, float layerDepth);

                    sprite.Draw(texture, playerHitbox, null, Color.White,0,Vector2.Zero,SpriteEffects.FlipVertically,0);

                    sprite.Draw(texture, playerHitbox, null, Color.White, 0, Vector2.Zero, SpriteEffects.FlipVertically, 0);

                    break;
            }

            //position of the oxygen box
            Rectangle oxygenBox = new Rectangle(playerHitbox.X + 540, 15, oxygenMeter.Width, oxygenMeter.Height);

            //oxygen meter scaling
            Rectangle oxygenBar = new Rectangle(playerHitbox.X + 539, 13 + (oxygenMeter.Height - (int)(oxygenMeter.Height * (currentOxygen / maxOxygen))), oxygenMeter.Width, (int)(oxygenMeter.Height * (currentOxygen / maxOxygen)));


            //Lives indicator position
            Vector2 livesPosition = new Vector2(playerHitbox.X - 680, 730);


            //Draws the "O2" text
            sprite.Draw(oxygenText, oxygenBox, Color.White);

            //Drawing the oxygen meter
            sprite.Draw(oxygenMeter, oxygenBar, Color.Green);

            //Draws the oxygen box
            sprite.Draw(oxygenContainer, oxygenBox, Color.White);

            //Text for life (make sure it's not 0)
            int numLifeNew = numLives;

            //Draws the lives indicator
            sprite.Draw(livesText, livesPosition, Color.White);
            sprite.DrawString(lives, " " + numLifeNew, new Vector2(livesPosition.X + 80, livesPosition.Y + 12), Color.Red);

            //Gravity Alert
            if (isWarned == true)
            {
                sprite.Draw(gravityWarning, new Vector2(playerHitbox.X - 120, 15), Color.White);
            }
        }
    }
}
