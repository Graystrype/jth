﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
/*
 * Malcolm Lambrecht, Tile Class, Journey to Hermes
 * Used to store data about a tile for the map
 */
namespace JourneyToHermes
{
    class Tile
    {
        Rectangle hitbox;
        int xCoord;
        int yCoord;
        Texture2D texture;
        int rotateBy; //number of degrees clockwise to rotate the texture when drawn, has to be a multiple of 90
        bool isDeadly = false;
        bool isFinalCp = false;
        public int dialogueNum = 0;
        public bool firstTouch = true;

        public bool IsFinalCheckpoint
        {
            get { return isFinalCp; }
            set { isFinalCp = value; }
        }
        public bool IsDeadly
        {
            get { return isDeadly; }
            set { isDeadly = value; }
        }

        public Rectangle Hitbox
        {
            get { return hitbox; }
            set { hitbox = value; }
        }

        public int RotateBy
        {
            get { return rotateBy; }
            set { rotateBy = value; }
        }

        public Texture2D Texture
        {
            get { return texture; }
            set { texture = value; }
        }

        public int XCoord
        {
            get { return xCoord; }
            set { xCoord = value; }
        }

        public int YCoord
        {
            get { return yCoord; }
            set { yCoord = value; }
        }

        public Tile(int xPos, int yPos)
        {
            XCoord = xPos;
            YCoord = yPos;
        }
    }
}
